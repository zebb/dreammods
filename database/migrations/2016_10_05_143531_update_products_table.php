<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('name')->unique()->change();
            $table->longtext('description')->change();
            $table->float('price')->change();
            $table->integer('file_id')->nullable()->change();
            $table->string('image')->default('http://placehold.it/360x230')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
             $table->dropColumn(['file_id', 'image', 'name', 'description','price']);
        });
    }
}
