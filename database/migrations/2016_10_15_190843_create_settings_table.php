<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stripe_secret')->nullable();
            $table->string('stripe_publish')->nullable();
            $table->string('stripe_image')->default('/store.jpg');
            $table->string('stripe_msg')->nullable();
            $table->string('site_name')->default("Site Name");
            $table->string('site_bg_color')->default("#ffffff");
            $table->string('site_nav_color')->default("#cccccc");
            $table->string('site_banner')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
