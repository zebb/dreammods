<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'is_admin' => 0,
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Product::class, function (Faker\Generator $faker) {
  //  static $password;

    return [
        'name' => $faker->name,
        'price' => $faker->randomFloat(2, 50, 500),
        'description' => $faker->text,

    ];
});


$factory->define(App\Cart::class, function (Faker\Generator $faker) {
    //static $password;

    return [
        'product_id' => $faker->numberBetween(1,10),
        'quantity' => $faker->numberBetween(1,3),

    ];
});
