<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

$settings = \App\Settings::first();
if ($settings) {
  Route::get('/', 'HomeController@index');
}else{
  Route::get('/', function() {
    return view("init");
  });
}

Auth::routes();
Route::get('/colorpicker', function(){
  return view('layouts.colorpicker');
});
Route::post('/downloads/get', 'Downloads@get');
Route::post('/init', 'AdminController@init');

Route::get('/cart', 'CartController@get');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/settings', 'AdminController@settings');
Route::get('/admin/dashboard', 'AdminController@dashboard');
Route::get('/admin/products', 'AdminController@products');
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/add_product', 'AdminController@addProductView');

Route::post('/admin/users/delete/{id}', 'AdminController@users_delete');
Route::post('/create_product', 'AdminController@create_product');
Route::post('/admin/settings/stripe', 'AdminController@save_stripe');
Route::post('/admin/settings/site', 'AdminController@save_site');
Route::post('/upload/files/{id}', 'Uploads@files');
Route::get('/downloads', 'Downloads@show');

Route::get('/addToCart/{id}', 'CartController@addToCart');

Route::post('/editQuantity', "CartController@update")->name('editQuantity');
Route::post('/bitcoin' , "CartController@bitcoin")->name('bitcoinCheckout');

/*Route::post('/editQuantity', function(\Illuminate\Http\Request $request) {
  return response()->json(['message' => $request['quantity']]);
})->name('editQuantity');*/



Route::post('/', 'CartController@purchase');
