<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Auth;
use App\File;

class Downloads extends Controller
{
    //
    public function get(Request $request) {
      if(Auth::check()){

      $path = base_path('storage/app/downloads/'.substr($request->path, 10));
      return Response::download($path, $request->name);

     }else {
       abort(500);
     }
    }




    public function show() {
      if(Auth::check()){
         $downloads = Auth::user()->files()->get();
         return view('downloads')->with('downloads', $downloads);
      }else {
       abort(500);
     }
    }
}
