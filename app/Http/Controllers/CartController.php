<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Cart;
use App\User;
use App\Settings;
use \Bitpay\PrivateKey;

class CartController extends Controller
{


    public function get() {


        //return;

        $cart = Auth::user()->cart;
        return view('cart')->with('cart',$cart);
    }

    public function addToCart() {

      $item = Auth::user()->cart()->where('product_id','=', request()->id);
      if (!$item->exists()) {
        $cart = new Cart;
        $cart->user_id = Auth::user()->id;
        $cart->product_id = request()->id;
        $cart->quantity = 1;
        $cart->save();
      }else {
        $quantity = $item->first()->quantity;
        $item->update(['quantity' => $quantity + 1]);
      }
      return redirect('/cart');
    }


    public function purchase(Request $request) {




      $secret = decrypt(Settings::first()->stripe_secret);
      User::setStripeKey($secret);

      //return dd($request->stripeToken);
      $total = Auth::user()->cart->sum( function($cart) {
        return $cart->product->price * $cart->quantity;
      });

      Auth::user()->charge($total * 100, ['source' => $request->stripeToken]);

      $cart = Auth::user()->cart();
      $items = $cart->get();
      foreach ($items as $item){
        $quantity = $item->quantity;
        for ($i=0; $i < $quantity; $i++) {
          $file = $item->product()->first()->files()->where('user_id','=',null)->first();
          $file->user_id = Auth::user()->id;
          $file->save();
        }
      }
      $cart->delete();
      return redirect('/');
    }




    public function update(Request $request) {
      $product = Auth::user()->cart()->where('product_id','=', $request->id)->first();
      $product->quantity = $request->quantity;
      $product->save();

      $subTotal = Auth::user()->cart->sum( function($cart) {
        return $cart->product->price * $cart->quantity;
      });
      return response()->json([
        'subTotal' => $subTotal,
        'id' => $request->id,
        'total' => $product->product->price * $product->quantity
      ]);
    }


    public function bitcoin(Request $request) {
      $privateKey =new PrivateKey('/tmp/bitpay.pri');
      $privateKey->generate();
      $publicKey = new \Bitpay\PublicKey('/tmp/bitpay.pub');
      $publicKey->setPrivateKey($privateKey);
      $publicKey->generate();
      $storageEngine = new \Bitpay\Storage\FilesystemStorage();
      $storageEngine->persist($privateKey);
      $storageEngine->persist($publicKey);

            /**
       * To load keys that you have previously saved,
       * you must use the same storage engine. You
       * also must specify the same location for each
       * key you want to load.
       */
      $storageEngine = new \Bitpay\Storage\FilesystemStorage();

      $privateKey = $storageEngine->load('/tmp/bitpay.pri');
      $publicKey = $storageEngine->load('/tmp/bitpay.pub');

      /**
       * Generate our SIN:
       */
      $sin = \Bitpay\SinKey::create()->setPublicKey($publicKey)->generate();

      /**
       * Create the client:
       */
      $client = new \Bitpay\Client\Client();

      /**
       * The network is either livenet or testnet. You
       * can also create your own as long as it
       * implements the NetworkInterface. In this
       * example we will use testnet.
       */
      $network = new \Bitpay\Network\Testnet();

      /**
       * The adapter is what will make the calls to
       * BitPay and return the response from BitPay.
       * This can be updated or changed as long
       * as it implements the AdapterInterface.
       */
      $adapter = new \Bitpay\Client\Adapter\CurlAdapter();

      /**
       * Now all the objects are created and we can
       * inject them into the client.
       */
      $client->setPrivateKey($privateKey);
      $client->setPublicKey($publicKey);
      $client->setNetwork($network);
      $client->setAdapter($adapter);

      /**
       * Paste the pairing code you generated
       * in your merchant dashboard in STEP 2:
       */
      $pairingCode = 'rhZ1c8Y';

      $token = $client->createToken(
       array(
       'pairingCode' => $pairingCode,
       'label' => 'Some description...',
       'id' => (string) $sin,
       )
      );

      /**
       * Now persist (save) the token obtained OR,
       * after it's created initially, you can call the
       * getTokens() method to retrieve all tokens
       * associated with your key.
       */
      $persistThisValue = $token->getToken();

      echo 'Token obtained: ' . $persistThisValue . PHP_EOL;


      /**
       * Same setup as before...
       */
      $storageEngine = new \Bitpay\Storage\FilesystemStorage();

      $privateKey = $storageEngine->load('/tmp/bitpay.pri');
      $publicKey = $storageEngine->load('/tmp/bitpay.pub');

      $client = new \Bitpay\Client\Client();
      $network = new \Bitpay\Network\Testnet();
      $adapter = new \Bitpay\Client\Adapter\CurlAdapter();

      $client->setPrivateKey($privateKey);
      $client->setPublicKey($publicKey);
      $client->setNetwork($network);
      $client->setAdapter($adapter);

      /**
       * The last object that must be injected into
       * the client object is the token object. If
       * you didn't persist (save) it previously,
       * you can make a call to getTokens() and
       * retrieve the specific token value used in
       * the setToken() method parameter.
       */
      $token = new \Bitpay\Token();

      $token->setToken('qweRQWER1234123qeqwerqwerQWERQWERQWER13412341234');

      $client->setToken($token);

      /**
       * Create an Invoice object now. You can check
       * the InvoiceInterface for possible methods you
       * can use on this object - FYI.
       */
      $invoice = new \Bitpay\Invoice();

      /**
       * Create an Item object to store the details
       * of the item on your invoice. Then inject
       * the Item object into the Invoice object.
       */
      $item = new \Bitpay\Item();

      $item->setCode('skuNumber');
      $item->setDescription('General Description of Item');
      $item->setPrice('1.99');

      $invoice->setItem($item);

      /**
       * BitPay supports many different currencies.
       * and most shopping cart applications have a
       * defined set of currencies that can be used.
       * Call the setCurrency() method on the Invoice
       * object to specify the currency you wish to
       * use for this invoice. For the current list
       * of currencies supported by BitPay, see:
       * https://test.bitpay.com/bitcoin-exchange-rates
       * Also, if you wish to use rate data in your app,
       * see https://bitpay.com/api/rates for the rates
       * in an easily consumable JSON format.
       */
      $invoice->setCurrency(new \Bitpay\Currency('USD'));

      /**
       * Now create our invoice...
       */
      $client->createInvoice($invoice);

      /**
       * Simply displaying the invoice info here. In
       * your application, you'd redirect the user
       * to this URL so they can pay this invoice.
       * If you want to embed this invoice in an
       * iFrame, append &view=iframe to the URL
       * and specify this as the src parameter in
       * the iFrame tag itself. For more information
       * on displaying invoices, see:
       * https://bitpay.com/docs/display-invoice
       */
      return 'Success! Created invoice "' . $invoice->getId() . '". See ' . $invoice->getUrl() . PHP_EOL;


      return "Yay Bitcoin";
    }






}
