<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Storage;
use App\File;
use Session;

class Uploads extends Controller
{
    public function files(Request $request, $id) {

      $files = $request->file('file');
      foreach($files as $file){
        $name = $file->getClientOriginalName();
        $path = Storage::disk('local')->putFile('downloads', $file);

        $db = new File;
        $db->url = $path;
        $db->product_id = $id;
        $db->name = encrypt($name);
        if ($db->save()){
          Session::flash('file_success', 'Your file(s) has been uploaded successfully!');
        }
      }

      return back();
    }
}
