<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User;
use Input;
use Session;
use App\Product;
use Image;
use Storage;
use App\Settings;

class AdminController extends Controller
{
    public function index() {
      if ( Auth::user()->is_admin ){
        return view('admin.settings');
      }else {
        abort(404);
      }

    }

    public function settings() {
      if ( Auth::user()->is_admin ){
        return view('admin.settings');
      }else {
        abort(404);
      }
    }

    public function dashboard() {
      if ( Auth::user()->is_admin ){
        return view('admin.dashboard');
      }else {
        abort(404);
      }
    }


    public function products() {
      if ( Auth::user()->is_admin ){
        $products = Product::get();
        return view('admin.products')->with('products', $products);
      }else {
        abort(404);
      }
    }



    public function users() {
      if ( Auth::user()->is_admin ){

        $data = [
          'users' => User::where('is_admin','=',0)->get(),
        ];

        return view('admin.users')->with('data', $data);
      }else {
        abort(404);
      }
    }



    public function addProductView() {
      if ( Auth::user()->is_admin ){
        return view('admin.create_product');
      }else {
        abort(404);
      }
    }



    public function create_product(Request $request) {

      $this->validate($request, [ //Validates the input with regex
        'name'=> 'required|max:50|unique:products',
        'description' => 'required|filled',
        'price' => 'required|filled',
        'image' => 'required|image'
      ]);
      $path = $request->file('image')->store('image');
      $url = Storage::url($path);
      //$contents = Storage::get($path);

      //$upload = Storage::disk('public')->put($url, $contents);
      //return $upload;


      $product = new Product;
      $product->name = $request->name;
      $product->description = $request->description;
      $product->price = $request->price;
      $product->image = $url;

      if ($product->save()){
        Session::flash('product_success', 'Your product has been created successfully!');
      }

      return back();
    }



    public function save_stripe(Request $request) {



      if(Settings::first()) { //check to see if there are settings already submitted.
        $this->validate($request, [
         'stripe_msg' => 'max:36'
        ]);
        $settings = Settings::first();
      }else{
        $this->validate($request, [
         'secret' => 'required',
         'publish' => 'required',
         'stripe_msg' => 'required|max:36'
        ]);
        $settings = new Settings;
      }


      $settings->stripe_secret = encrypt($request->secret);
      $settings->stripe_publish = encrypt($request->publish);
      $settings->stripe_msg = $request->stripe_msg;
      $settings->save();

      return back();
    }

    public function save_site(Request $request) {

      if(Settings::first()) { //check to see if there are settings already submitted.
        $settings = Settings::first();
      }else{
        $settings = new Settings;
      }

        $settings->site_name = $request->site_name;
        $settings->site_bg_color = $request->site_bg_color;
        $settings->site_nav_color = $request->site_nav_color;
        $settings->site_banner = $request->site_banner;
        $settings->save();

      return back();
    }


    public function init(Request $request){

      $this->validate($request, [ //Validates the input with regex
        'email'=> 'required|email|max:50|unique:users',
        'name' => 'required',
        'password' => 'required|min:10',
        'site_name' => 'required',
        'stripe_secret' => 'required',
        'stripe_publish' => 'required',
      ]);

      $user = new User;
      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = bcrypt($request->password);
      $user->is_admin = 1;
      $user->save();


      $settings = new Settings;
      $settings->site_name = $request->site_name;
      $settings->stripe_secret = encrypt($request->stripe_secret);
      $settings->stripe_publish = encrypt($request->stripe_publish);
      $settings->save();

      Auth::login($user);
      return redirect('/');
    }






    public function users_delete() {
      if ( Auth::user()->is_admin ){
        //return request()->id;
        $user = User::find(request()->id);
        $user->delete();

        return back();
      }else {
        abort(404);
      }
    }
}
