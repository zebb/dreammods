<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
      'deleted_at',
      'trial_ends_at',
      'subscription_ends_at'
    ];

    protected $softDelete = true;

    //protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    public function cart(){
      return $this->hasMany('App\Cart');
    }
    public function files(){
      return $this->hasMany('App\File');
    }
}
