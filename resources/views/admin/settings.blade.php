@extends('admin.index')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>There was a problem changing your settings.</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<h2 class="text-center" style="padding-bottom: 15px;margin-bottom: 20px">Admin Settings</h2><hr>






<div class="col-lg-6 " style="margin-bottom: 30px;">
<div class="card card-block" style="padding: 40px;">
  <h4 class="card-title text-right">Stripe API</h4>
  <hr>
  <form action="/admin/settings/stripe" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="secret">Stripe Secret Key</label>
      <input type="text" name="secret" class="form-control" placeholder="sk_live_**********">
    </div>

    <div class="form-group">
      <label for="publish">Stripe Publishable Key</label>
      <input type="text" name="publish" class="form-control"  placeholder="pk_live_**********">
    </div>

    <div class="form-group">
      <label for="stripe_msg">Stripe Checkout Message</label>
      <input type="text" name="stripe_msg" class="form-control"  placeholder="ex. We will never share your info.">
    </div>

    <div class="form-group">
      <label>Stipe Checkout Image</label><br>
      <div class="input-group">

          <label class="input-group-btn">
              <span class="btn btn-primary">
                  Browse&hellip; <input type="file" name='stipe_image' style="display: none;" multiple>
              </span>
          </label>
          <input type="text" class="form-control" readonly>
      </div>
    </div>

    <button type="submit" class="btn btn-primary pull-right" style="margin-top: 30px;width:100%">Save</button>
  </form>
</div>
</div>






<div class="col-lg-6 " style="margin-bottom: 30px;">
<div class="card card-block" style="padding: 40px;">
  <h4 class="card-title text-right">Site</h4>
  <hr>
  <form action="/admin/settings/site" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="name">Site Name</label>
      <input type="text" name="site_name" class="form-control"  placeholder="What is the name of your site?">
    </div>
    <div class="form-group">
      <label for="bg-color">Background Color</label>
      <input type="color" name="site_bg_color" class="form-control"  placeholder="#ff0000">
    </div>
    <div class="form-group">
      <label for="bg-color">Navigation Color</label>
      <input type="color" name="site_nav_color" class="form-control"  placeholder="#ff0000">
    </div>
    <div class="form-group">
      <label for="bg-color">Site Banner</label>
      <input type="color" name="site_banner" class="form-control"  placeholder="#ff0000">
    </div>



    <button type="submit" class="btn btn-primary pull-right" style="margin-top: 30px; width:100%">Save</button>
  </form>
</div>
</div>






<script type="text/javascript">


setTimeout( function(){$('.flash-message').slideUp();} , 2000);
  $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });

  });
</script>

@endsection
