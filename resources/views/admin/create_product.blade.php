@extends('admin.index')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>There was a problem creating your product.</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::has('product_success'))
  <div class="alert alert-success flash-message">
    {{Session::get('product_success')}}
  </div>
@elseif (Session::has('product_failure'))
<div class="alert alert-danger flash-message">
  {{Session::get('product_failure')}}
</div>
@endif

<h2 class="text-center" style="padding-bottom: 15px;margin-bottom: 20px">Add a Product</h2><hr>



<div class="col-lg-10 offset-lg-1 " style="margin-bottom: 30px;">
<div class="card card-block" style="padding: 40px;">
<form action="/create_product" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="name">Product name</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="The name of your product.">
  </div>

  <div class="form-group">
    <label for="description">Product description</label>
    <textarea class="form-control" name="description" id="description" rows="3" placeholder="Write a description of your product here."></textarea>
  </div>



  <div class="form-group">
    <label for="price">Product price</label>
    <div class="input-group" style="width: 25%;">
      <div class="input-group-addon">$</div>
      <input type="number" class="form-control " name="price" id="price" placeholder="Amount">

    </div>
  </div>

  <div class="form-group">
    <label>Product image</label><br>
    <div class="input-group">

        <label class="input-group-btn">
            <span class="btn btn-primary">
                Browse&hellip; <input type="file" name='image' style="display: none;" multiple>
            </span>
        </label>
        <input type="text" class="form-control" readonly>
    </div>
  </div>



  <button type="submit" class="btn btn-primary pull-right" style="margin-top: 30px; width:100%">Add Product</button>
</form>
</div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
  setTimeout( function(){$('.flash-message').slideUp();} , 2000);
</script>
<script src="/js/file_upload.js"></script>

@endsection
