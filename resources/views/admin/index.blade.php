<!DOCTYPE html>
<html lang="en">
<head>
    @include('head')
    <link href="/css/simple-sidebar.css" rel="stylesheet">

</head>
<body class="">
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <i class="fa fa-angle-left" aria-hidden="true"></i> Home
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                          <li class="dropdown">
                              <a href="#"  data-toggle="dropdown" role="button" aria-expanded="false">
                                  {{ Auth::user()->name }} <span class="caret"></span>
                              </a>
                            <ul class="dropdown-menu" role="menu">
                              <li>
                                  <a href="{{ url('/logout') }}"
                                      onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                      <i class="fa fa-sign-out" aria-hidden="true"></i> Logout
                                  </a>

                                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                  </form>
                              </li>
                            </ul>
                          </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>


















        <div id="wrapper">

                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <ul class="sidebar-nav">


                        <li>
                            <a href="/admin/settings">

                           <i class="fa fa-cog" aria-hidden="true"></i>    Admin Settings
                          </a>

                        </li>

                        <!-- <li>
                            <a href="/admin/dashboard">

                           <i class="fa fa-tachometer" aria-hidden="true"></i>    Dashboard
                          </a>

                        </li> -->



                        <li>
                            <a href="/admin/products">

                           <i class="fa fa-database" aria-hidden="true"></i>    Products
                          </a>

                        </li>



                        <li>
                            <a href="/admin/users">

                           <i class="fa fa-users" aria-hidden="true"></i>    Users
                          </a>

                        </li>


                        <li>
                            <a href="/admin/add_product">

                           <i class="fa fa-plus" aria-hidden="true"></i>    Add Product
                          </a>

                        </li>




                    </ul>
                </div>
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /#page-content-wrapper -->

            </div>
            <!-- /#wrapper -->


    </div>

    <!-- Scripts -->

    <script src="/js/app.js"></script>
</body>
</html>
