@extends('admin.index')

@section('content')


<style media="screen">
  .list-group-item:hover {
    text-decoration: none;
    color: #555;
    background-color: #eee;
  }
  h5 {
    display: inline-block;
    font-weight: bold;
    margin-right: 10px;
  }
</style>
<h2 class="text-center" style="padding-bottom: 15px;margin-bottom: 20px">Users</h2><hr>

<ul class="list-group">
  @foreach($data['users'] as $user)
  <li class="list-group-item">
      {{$user->email}}

      <a href='/admin/users/delete/{{$user->id}}' class="btn btn-sm btn-danger pull-right" style="line-height: 1; margin: 0 5px"
          onclick="event.preventDefault();
                   document.getElementById('delete-form').submit();">
        Ban
      </a>

      <form id="delete-form" action='/admin/users/delete/{{$user->id}}' method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
      <button type="button" class="btn btn-info btn-sm pull-right" style="line-height: 1; margin: 0 5px" data-toggle="collapse" data-target="#id{{$user->id}}">View Info</button>

      <div id="id{{$user->id}}" class="collapse">
        <ul>
          <li><h5>Name:</h5> {{$user->name}}</li>
          <li><h5>Created:</h5> {{$user->created_at}}</li>
        </ul>


      </div>

  </li>
  @endforeach
</ul>

@endsection
