@extends('admin.index')

@section('content')
<style media="screen">
  p.down {
    margin: 0;
    position: absolute;
    width: 100%;
    text-align: center;
    left: 0;
    border-top: 1px solid rgba(0,0,0,.125);
    padding-top: 4px;
    font-size: 20px;
    color: rgb(99, 107, 111);
    bottom: 0;
  }
</style>

@if (Session::has('file_success'))
  <div class="alert alert-success flash-message">
    {{Session::get('file_success')}}
  </div>
@endif

<h2 class="text-center" style="padding-bottom: 15px;margin-bottom: 20px">Products</h2><hr>
<div class="col-lg-6 " style="margin-bottom: 30px;">
  @foreach($products as $product)
    <div class="card card-block" style="padding: 40px;" >
      <h4 class="card-title text-right">{{$product->name}}</h4>



      <div id="id{{$product->id}}" class="collapse">
        <form action="/upload/files/{{$product->id}}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Product Files</label><br>
            <div class="input-group">

                <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Browse&hellip; <input type="file" name='file[]' style="display: none;" multiple>
                    </span>
                </label>
                <input type="text" class="form-control" readonly>
            </div>
          </div>
          <button type="submit" class="btn btn-primary pull-right" style="margin-bottom:10px; width:100%">Add Files</button>
        </form>
      </div>

      <p class="down text-center" data-toggle="collapse" data-target="#id{{$product->id}}">
        <i class='fa fa-angle-double-down'></i>
      </p>


    </div>
  @endforeach
</div>




<script src="/js/file_upload.js"></script>
<script type="text/javascript">
  setTimeout( function(){$('.flash-message').slideUp();} , 2000);
</script>
@endsection
