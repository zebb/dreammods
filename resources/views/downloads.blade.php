@extends('layouts.app')

@section('content')

<style media="screen">
.table-striped>tbody>tr:nth-of-type(odd) {
  background-color: #ffffff;
}
table {
  -webkit-box-shadow: 0px 2px 4px -2px #aaa;
-moz-box-shadow: 0px 2px 4px -2px #aaa;
box-shadow: 0px 2px 4px -2px #aaa;
border: 1px solid #ddd;
border-bottom: 1px solid #ccc;
}
</style>

<div class="container">
  <h2 class="text-center" style="padding-bottom: 15px;margin-bottom: 20px">My Downloads</h2><hr style="border-color:#666e72">
  <table class="table table-striped">

  <tbody>
    @foreach($downloads as $download)
      <tr>
        <td>{{decrypt($download->name)}}</td>
        <td>
          <form class="" action="/downloads/get" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="path" value="{{$download->url}}">
            <input type="hidden" name="name" value="{{decrypt($download->name)}}">
            <button type="submit" class="btn btn-primary pull-right"> Download</button>
          </form>
        </td>
      </tr>
    @endforeach


  </tbody>
</table>
</div>

@endsection
