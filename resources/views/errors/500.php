<!DOCTYPE html>
<html>
    <head>
        <title>ERROR</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,900" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 300;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 40px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Could not find download.</div>
                <p>
                  Make sure you are logged in on the <a href="/">main site</a>  and you are using the correct download link.
                </p>
            </div>
        </div>
    </body>
</html>
