
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>
    <?php
      $settings = \App\Settings::first();
      echo $settings->site_name;
    ?>
  </title>

    <!-- Favicon Stuff -->



  <!-- Font Awesome -->
  <link rel="stylesheet" href="/css/font-awesome.min.css" media="screen" title="no title">

  <!-- Styles -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
  <link href="/css/app.css" rel="stylesheet">


  <!-- Scripts -->
   <script>
       window.Laravel = <?php echo json_encode([
           'csrfToken' => csrf_token(),
       ]); ?>
   </script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
   <script src="https://js.braintreegateway.com/web/3.3.0/js/client.min.js"></script>
   <script src="https://js.braintreegateway.com/web/3.3.0/js/paypal.min.js"></script>
