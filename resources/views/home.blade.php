@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">


          @foreach($data['products'] as $product)
          <div class="col-sm-4 col-lg-4 col-md-4" style="margin-bottom: 30px;">
            <div class="card">
              <img class="card-img-top" src="{{$product->image}}" alt="Card image cap">
              <div class="card-block">
                <h4 class="card-title">{{$product->name}}<span class="card-price pull-right">${{$product->price}}</span></h4>
                <p class="card-text">{{$product->description}}</p>
                @if(Auth::check())
                  <?php $file = \App\File::where('product_id','=',$product->id)->where('user_id','=',null)->get();
                        $inCart = Auth::user()->cart()->where('product_id','=',$product->id);
                        if ($inCart->exists()){
                          $quantityWanted = $inCart->first()->quantity;
                        }else {
                          $quantityWanted = 0;
                        }

                    //echo $file;
                  ?>
                  @if($file->count() > 0 && $quantityWanted < $file->count() )
                    <a href="/addToCart/{{$product->id}}" class="btn btn-primary text-center" style="width: 100%;"> <span style="margin-right: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i></span>  Add to cart</a>
                  @else
                    <a href="/addToCart/{{$product->id}}" class="btn btn-primary text-center disabled" style="width: 100%;"> <span style="margin-right: 10px"><i class="fa fa-ban" aria-hidden="true"></i></span>  Sold out</a>
                  @endif

                @else


                  <script src="https://checkout.stripe.com/checkout.js"></script>
                  <a href="/login" class="btn btn-primary text-center" style="width: 100%;"><span style="margin-right: 10px"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span>  Buy now</a>


                @endif
              </div>
            </div>

          </div>
          @endforeach


    </div>




@endsection
