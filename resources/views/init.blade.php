
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Site Setup</title>

    <!-- Favicon Stuff -->



  <!-- Font Awesome -->
  <link rel="stylesheet" href="/css/font-awesome.min.css" media="screen" title="no title">

  <!-- Styles -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
  <link href="/css/app.css" rel="stylesheet">


  <!-- Scripts-->
  <script>
      window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
      ]); ?>
  </script>
  <script src="/js/jquery.min.js"></script>








<div class="container">
    <div class="row" style="margin-top:10px;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Site Setup</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/init">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus >

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('site_name') ? ' has-error' : '' }}">
                            <label for="site_name" class="col-md-4 control-label">Site Name</label>

                            <div class="col-md-6">
                                <input id="site_name" type="site_name" class="form-control" name="site_name" value="{{ old('site_name') }}" required>

                                @if ($errors->has('site_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('site_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('stripe_secret') ? ' has-error' : '' }}">
                            <label for="stripe_secret" class="col-md-4 control-label">Stripe Secret Key</label>

                            <div class="col-md-6">
                                <input id="stripe_secret" type="stripe_secret" class="form-control" name="stripe_secret" value="{{ old('stripe_secret') }}" required>

                                @if ($errors->has('stripe_secret'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stripe_secret') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('stripe_publish') ? ' has-error' : '' }}">
                            <label for="stripe_publish" class="col-md-4 control-label">Stripe Publishable Key</label>

                            <div class="col-md-6">
                                <input id="stripe_publish" type="stripe_publish" class="form-control" name="stripe_publish" value="{{ old('stripe_publish') }}" required>

                                @if ($errors->has('stripe_publish'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stripe_publish') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>







                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create Site
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
