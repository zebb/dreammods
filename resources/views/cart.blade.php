@extends('layouts.app')

@section('content')
<style media="screen">
.table-striped>tbody>tr:nth-of-type(odd) {
  background-color: #ffffff;
}
table {
  -webkit-box-shadow: 0px 2px 4px -2px #aaa;
-moz-box-shadow: 0px 2px 4px -2px #aaa;
box-shadow: 0px 2px 4px -2px #aaa;
border: 1px solid #ddd;
border-bottom: 1px solid #ccc;
}
</style>


  <div class="container">

    <table class="table table-striped">
      <thead >
        <tr>
          <th class="text-center">#</th>
          <th class="text-left">Product</th>
          <th class="text-left">Price</th>
          <th class="text-center">Quantity</th>
          <th class="text-right">Total</th>
        </tr>
      </thead>
    <tbody>
      <?php
        $total = 0;
        $count = 1;
      ?>
      @foreach($cart as $cart)
        <tr>
          <th scope="row" class="text-center">{{$count}}</th>
          <td>{{$cart->product->name}}</td>
          <td>${{$cart->product->price}}</td>
          <td class="text-center">
            <input type="number" class="quantity" data-id="{{$cart->product_id}}" value="{{$cart->quantity}}" style="width: 40px; text-align: right;">
            </td>
          <td class="text-right" id="total-{{$cart->product_id}}" style="width: 100px !important">${{$cart->product->price * $cart->quantity}}</td>

        </tr>
        <?php $total += $cart->product->price * $cart->quantity; $count+=1?>
      @endforeach
    </tbody>
    <thead>
      <tr class="">
        <th></th>
        <th colspan="3"><strong>Sub Total</strong></th>

        <th class="text-right" id="subTotal">

        ${{$subTotal = Auth::user()->cart->sum( function($cart) {
          return $cart->product->price * $cart->quantity;
        })}}
        </th>
      </tr>
    </thead>

  </table>
  <script src="https://checkout.stripe.com/checkout.js"></script>







<form action="/" method="post">
  {{ csrf_field() }}


        <button
            class="btn btn-primary text-center"
            style="width: 100%;"
            type="submit"
            id="checkout-btn"

            data-key="{{decrypt(\App\Settings::first()->stripe_publish)}}"
            data-image="{{\App\Settings::first()->stripe_image}}"
            data-amount="{{$subTotal * 100}}"
            data-name="{{ config('app.name', 'Laravel') }}"
            data-email="{{Auth::user()->email}}"
            data-description="{{\App\Settings::first()->stripe_msg}}"
            data-zip-code="true"
            data-billing-address="true"
            data-currency="USD"
            data-bitcoin=true
            data-locale="auto"
        >
        <span style="margin-right: 10px"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span>  Checkout
        </button>

        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script>
        $(document).ready(function() {
            $('#checkout-btn').on('click', function(event) {
                event.preventDefault();
                var $button = $(this),
                    $form = $button.parents('form');
                var opts = $.extend({}, $button.data(), {
                    token: function(result) {
                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                    }
                });
                StripeCheckout.open(opts);
            });
        });
        </script>
</form>


<form class="" action="{{ route('bitcoinCheckout') }}" method="post">
  {{ csrf_field() }}
  <button type="submit" class="btn btn-primary text-center">Pay with BTC</button>
</form>


<script type="text/javascript">
  var token = '{{ Session::token() }}'
  var url = '{{ route('editQuantity') }}'
  $( ".quantity" ).change(function() {
    //console.log($(this).val());
    $.ajax({
       type : 'POST',
       url  : url,
       data : {
         quantity: $(this).val(),
         id: $(this).data('id'),
         _token: token
       },
    }).done(function (msg) {
      var subTotal = msg['subTotal'];
      $('#subTotal').html('$'+subTotal.toFixed(2))
      console.log($('#subTotal').html());
      $('#checkout-btn').data('amount', subTotal*100)
      $("#total-"+msg['id']).html('$'+msg['total'].toFixed(2));
    });
  });



</script>








@endsection
